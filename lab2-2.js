function isIPAddress(ip) {
    var regexp = /(\d+\.){1,3}\d{4}/;
    var res = ip.match(regexp);
    if(! res ) return false;
    return res.input == res[0];
}

function findRGBA(text) {
	var regexp = /rgba\(([01]?\d\d?|2[0-4]\d|25[0-5])\,([01]?\d\d?|2[0-4]\d|25[0-5])\,([01]?\d\d?|2[0-4]\d|25[0-5])\,([1]|[0].[0-9]+)\)/im;
    var res = text.match(regexp);
    res = res ? res[0] : res;
    return res;
}

function findHexColor(text) {
    var regexp = /#([\da-f]{3}){1,2}/im;
    var res = text.match(regexp);
    res = res ? res[0] : res;
    return res;
}

function findTags(text, tag) {
    var regexp = new RegExp("<" + tag + "([^>]+)?>","gi");
    var res = text.match(regexp);
    return res;
}

function findPosNum(text) {
    var regexp = /\d+(\.\d+)?/mg;
    return text.match(regexp);
}

function findDates(text) {
    var regexp = /\d{3,4}-\d{2}-\d{2}/mg;
    return text.match(regexp);
}